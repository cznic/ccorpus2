# ccorpus2

Continuation of ccorpus but using embed.FS

## Installation

    $ go install modernc.org/ccorpus

## Documentation

[godoc.org/modernc.org/ccorpus2](http://godoc.org/modernc.org/ccorpus2)

## Builders

[modern-c.appspot.com/-/builder/?importpath=modernc.org%2fccorpus2](https://modern-c.appspot.com/-/builder/?importpath=modernc.org%2fccorpus2)
